// If we want to listen to key events, we can use the following code
function addTestingKeyboardListeners() {
  // We need keydown to capture special keys like shift (see keydownOnlyKeys)
  // We grab them and handle them here
  document.addEventListener("keydown", (event) => {
    // Ignore shift (so we can grab all shift + key combinations)
    if (keydownOnlyKeys.includes(event.key)) {
      console.log("keydown", event.key);
      event.preventDefault();
    }
  });

  // Most keys we can grab with keypress
  document.addEventListener("keypress", (event) => {
    if (event.key === "Shift") {
      console.log("shift keypress");
    }
    console.log("keypress", event.key);
    event.preventDefault();
  });
}

const keydownOnlyKeys = [
  "Alphanumeric", // caps lock
  "Backspace",
  "Control",
  "Alt",
  "Meta",
  "CapsLock",
  "Tab",
  "Escape",
  "ArrowUp",
  "ArrowDown",
  "ArrowLeft",
  "ArrowRight",
];
