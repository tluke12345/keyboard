import { vi } from "vitest";

// override scroll behavior
// @ts-expect-error just a test
global.window.scrollTo = vi.fn();
