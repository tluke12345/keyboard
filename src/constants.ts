/* COLORS */
export const kWhite = "#ffffff";
export const kLight100 = "#f8f9f8";
export const kLight200 = "#f1f3f1";
export const kLight300 = "#eaecea";
export const kLight400 = "#e3e6e3";
export const kLight500 = "#dde0dc";
export const kLight600 = "#d6dad4";
export const kLight700 = "#cfd4cd";
export const kLight800 = "#c8cdc6";
export const kLight900 = "#c1c7bf";
export const kLight = "#bac1b8";

export const kDark = "#2b303a";
export const kDark100 = "#272b34";
export const kDark200 = "#22262e";
export const kDark300 = "#1e2229";
export const kDark400 = "#1a1d23";
export const kDark500 = "#16181d";
export const kDark600 = "#111317";
export const kDark700 = "#0d0e11";
export const kDark800 = "#090a0c";
export const kDark900 = "#040506";
export const kBlack = "#000000";

export const kPrimary = "#d64933";
export const kSecondary = "#0c7c59";
export const kTertiary = "#58a4b0";

export const kGap1 = "var(--gap1)";
export const kGap2 = "var(--gap2)";
export const kGap3 = "var(--gap3)";

export const apiHost = "http://localhost";
export const apiPort = 3000;

export const keyToLabelMap: Record<string, string> = {
  Backspace: "⌫",
  Enter: "⏎",
  Shift: "⇧",
  Control: "Ctrl",
  Alt: "Alt",
  Meta: "⌘",
  CapsLock: "⇪",
  Backlight: "⌨",
  Alphanumeric: "⇪",
  Tab: "⇥",
  Escape: "⎋",
  ArrowUp: "↑",
  ArrowDown: "↓",
  ArrowLeft: "←",
  ArrowRight: "→",
};
