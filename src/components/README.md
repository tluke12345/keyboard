# Component / Element pattern

Sometimes we want a 'stateless' Element (HTMLElement) builder function.

Other times we want a 'stateful' Component class.

## Element builder function

- Caplitalized name
- Returns an HTMLElement

```typescript
const Text = (text: string): HTMLElement => {
  const el = document.createElement("div");
  el.textContent = text;
  return el;
};
```

## Component class

When we want to manage state, we use a class.

We can 'upgrade' the Element builder function to a class.

- class name is XXXComponent
- implements Component interface (has a html method)
- provides a factory method (that looks like the Element builder function)

```typescript
class TextComponent implements Component {
  private el: HTMLElement;
  constructor(text: string) {
    this.el = document.createElement("div");
    this.el.textContent = text;
  }
  html = () => this.el;
  static create = (text: string) => new TextComponent(text);
}

const Text = (text: string): HTMLElement => TextComponent.create(text).html();
```
