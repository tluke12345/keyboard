/*
 * Component interface
 *
 * A component is simply an object that can be converted to an HTMLElement.
 * If we need some UI element that holds state, we can create a component that
 * converts to html that reflects the state.
 */
export interface Component {
  html(): HTMLElement;
}
