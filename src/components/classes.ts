/*
 * @file: classes.ts
 * Simple HTMLElement 'css class' helper.
 *
 * NOTE:
 * In general, just access HTMLElement.classList directly.
 * This helper is only useful for:
 *
 * - handle undefined class lists -> applyClasses(element, undefined)
 * - multiple classes at once -> applyClasses(element, ["class1", "class2"])
 */

/* Add CSS classes to an element */
export const applyClasses = (element: HTMLElement, classes?: string[]) => {
  (classes ?? []).forEach((cls) => {
    if (cls === "") {
      return;
    }
    element.classList.add(cls);
  });
};
