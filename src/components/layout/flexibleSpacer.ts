export const FlexibleSpacer = (): HTMLElement => {
  const element = document.createElement("div");
  element.style.flex = "2";
  return element;
};
