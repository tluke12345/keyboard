import { applyStyles } from "../styles";

export const Row = (children: HTMLElement[]) => {
  const element = document.createElement("div");
  applyStyles(element, {
    display: "flex",
  });
  children.forEach((child) => {
    element.appendChild(child);
  });
  return element;
};
