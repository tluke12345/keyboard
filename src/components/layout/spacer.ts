export const Spacer = ({
  width,
  height,
}: { width?: string; height?: string } = {}): HTMLElement => {
  const element = document.createElement("div");
  if (width) {
    element.style.width = width;
  }
  if (height) {
    element.style.height = height;
  }
  return element;
};

export const FlexSpacer = (): HTMLElement => {
  const element = document.createElement("div");
  element.style.flex = "1";
  return element;
};
