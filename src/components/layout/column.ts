import { applyStyles } from "../styles";

export const Column = (children: HTMLElement[]) => {
  const element = document.createElement("div");
  applyStyles(element, {
    display: "flex",
    flexDirection: "column",
  });
  children.forEach((child) => {
    element.appendChild(child);
  });
  return element;
};
