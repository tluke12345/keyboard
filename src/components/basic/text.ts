/**
 * Simple text element builder.
 * @param text - The text to display.
 * @param classes - The classes to apply to the text.
 */
export function Text(text: string, {cssClass}: {cssClass?: string} = {}) {
  const element = document.createElement("div");
  if (cssClass) {
    element.classList.add(cssClass);
  }
  element.innerText = text;
  return element;
}
