import { replaceInDOM } from "../../utils/dom";

/*
 * Svg Element
 * @param path: string
 * @param height: number (pixels)
 * @param width: number (pixels)
 * @param altText: string
 * @returns <svg> tag element (cannot be given "style" or "class" to add CSS)
 *
 * Returns a dummy element and replaces it with the SVG when it's loaded.
 */
export const Svg = (
  path: string,
  {
    height,
    width,
    altText,
    color,
  }: { height?: number; width?: number; altText?: string; color?: string } = {},
) => {
  const element = document.createElement("svg");
  new Promise<void>((resolve) => {
    loadSvg(path).then((svg) => {
      replaceInDOM(element, svg);
      if (height) {
        svg.setAttribute("height", `${height}`);
      }
      if (width) {
        svg.setAttribute("width", `${width}`);
      }
      if (altText) {
        svg.setAttribute("aria-label", altText);
      }
      svg.setAttribute("fill", color || "currentColor");
      resolve();
    });
  });
  return element;
};

/*
 * Load SVG
 * @param path: string
 * @returns Promise<HTMLElement>
 */
const loadSvg = async (path: string): Promise<HTMLElement> => {
  try {
    const response = await fetch(path);
    if (!response.ok) {
      console.error(`Failed to fetch SVG: ${path}`);
      return brokenImageElement();
    }
    const svgText = await response.text();
    const parser = new DOMParser();
    const doc = parser.parseFromString(svgText, "image/svg+xml");
    if (doc.documentElement.tagName !== "svg") {
      // DOMParser doesn't always throw errors
      // Instead, it returns a document with an error message.
      console.error(`Failed to parse SVG: ${path}`);
      return brokenImageElement();
    }
    return doc.documentElement;
  } catch (e) {
    console.error(`Failed to Load SVG: ${path}`);
    return brokenImageElement();
  }
};

/*
 * Broken Image Element
 * @returns a link with an invalid src to show browser's broken image icon
 */
function brokenImageElement() {
  const img = document.createElement("img");
  img.src = "broken";
  img.width = 24;
  img.height = 24;
  return img;
}

/*
 * Svg Element
 * @param path: string
 * @param height: number (pixels)
 * @param width: number (pixels)
 * @param altText: string
 * @param color: string (default: "currentColor")
 * @returns HTMLDivElement (can be "style" or "class" to add CSS)
 */
export const SvgElement = (
  path: string,
  {
    height,
    width,
    altText,
    color,
  }: { height?: number; width?: number; altText?: string; color?: string } = {},
): HTMLElement => {
  const element = document.createElement("div");
  element.classList.add("svg-element");
  const svg = Svg(path, { height, width, altText, color });
  element.appendChild(svg);
  return element;
};

/*
 * Svg Icon
 * @param path: string
 * @param height: number (pixels, default 24, undefined to unset)
 * @param width: number (pixels, default 24, undefined to unset)
 * @param altText: string
 * @param cssClass: string (default: "icon")
 * @returns HTMLElement
 */
export const SvgIcon = (
  path: string,
  {
    height = 24,
    width = 24,
    altText,
    cssClass = "icon",
  }: {
    height?: number;
    width?: number;
    altText?: string;
    cssClass?: string;
  } = {},
): HTMLElement => {
  const svg = SvgElement(path, { height, width, altText });
  if (cssClass) {
    svg.classList.add(cssClass);
  }
  return svg;
};
