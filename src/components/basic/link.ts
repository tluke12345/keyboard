export function Link(
  url: string,
  { text, children }: { text?: string; children?: HTMLElement[] },
) {
  const element = document.createElement("a");
  element.href = url;
  if (text !== undefined) {
    element.innerText = text;
  }
  if (children !== undefined && children.length > 0) {
    element.append(...children);
  }
  return element;
}

export function TextLink(url: string, text: string) {
  return Link(url, { text });
}

export function ContainerLink(url: string, children: HTMLElement[]) {
  return Link(url, { children });
}
