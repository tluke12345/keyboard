/*
 * SelectInput
 * @param choices: string[]
 * @param selected: string
 * @param onChange: (value: string) => void
 * @returns HTMLSelectElement
 *
 * A select element that allows the user to choose from a (static) list of
 * options.
 */
export const SelectInput = (
  choices: string[],
  selected: string,
  onChange: (value: string) => void,
) => {
  const element = document.createElement("select");
  choices.forEach((choice) => {
    const option = document.createElement("option");
    option.value = choice;
    option.textContent = choice;
    if (choice === selected) {
      option.selected = true;
    }
    element.appendChild(option);
  });
  element.addEventListener("change", (e) => {
    const target = e.target as HTMLSelectElement;
    onChange(target.value);
  });
  return element;
};
