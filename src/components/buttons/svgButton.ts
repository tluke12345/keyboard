/*
 * Basic button component
 * @param text - text to display on the button
 * @param htmlElement - html element to display on the button
 * @param onClick - function to call when the button is clicked
 * @returns button element (HTMLElement)
 */
export const Button = ({
  text,
  htmlElement,
  onClick,
}: {
  text?: string;
  htmlElement?: HTMLElement;
  onClick: () => void;
}): HTMLElement => {
  const button = document.createElement("button");
  if (text) {
    button.textContent = text;
  }
  if (htmlElement) {
    button.appendChild(htmlElement);
  }
  button.addEventListener("click", onClick);
  return button;
};
