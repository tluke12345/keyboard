/*
 * ToggleButton
 *
 * A simple button element that has selected and highlighted states.
 */
export const ToggleButton = (
  label: string,
  selected: boolean,
  highlight: boolean,
  onClick?: (e: Event) => void,
) => {
  const element = document.createElement("button");
  element.textContent = label;
  if (onClick) {
    element.addEventListener("click", onClick);
  }
  if (selected) {
    element.classList.add("selected");
  } else {
    element.classList.remove("selected");
  }
  if (highlight) {
    element.classList.add("highlighted");
  } else {
    element.classList.remove("highlighted");
  }
  return element;
};
