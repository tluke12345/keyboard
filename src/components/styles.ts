/*
 * Styles type
 * Any style that you want to apply programmatically should be added here.
 */
export type Styles = {
  color?: string | null;
  backgroundColor?: string | null;
  display?: string | null;
  flexDirection?: string | null;
};

/*
 * Apply styles to an element
 * @param element - The element to apply the styles to
 * @param styles - The styles to apply
 *
 * Note: 'null' values will REMOVE the style ('' also works!)
 */
export const applyStyles = (element: HTMLElement, styles?: Styles) => {
  if (!styles) {
    return;
  }
  for (const [key, value] of Object.entries(styles)) {
    const hyphenatedKey = key.replace(/([A-Z])/g, "-$1").toLowerCase();
    if (value === null) {
      element.style.removeProperty(hyphenatedKey);
      continue;
    }
    element.style.setProperty(hyphenatedKey, value);
  }
};
