import { Keyboard } from "../models/Keyboard";
import { KeymapSet } from "../models/keymap";
import { KeyboardRepository } from "../repositories/keyboardRepository";
import { KeymapRepository } from "../repositories/keymapRepository";

// Names Load Callback
export type LoadNamesCallback = {
  onError: (msg: string) => void;
  onSuccess?: (names: string[]) => void;
  onLoading: () => void;
};

// Keyboard Load Callback
export type LoadKeyboardCallback = {
  onError: (msg: string) => void;
  onSuccess?: (keyboard: Keyboard) => void;
  onLoading: () => void;
};

// Keymap Load Callback
export type LoadKeymapCallback = {
  onError: (msg: string) => void;
  onSuccess?: (keymapSet: KeymapSet) => void;
  onLoading: () => void;
};

export class AppViewModel {
  #keyboardRepository: KeyboardRepository;
  #keymapRepository: KeymapRepository;

  constructor(
    keyboardRepository: KeyboardRepository,
    keymapRepository: KeymapRepository,
  ) {
    this.#keyboardRepository = keyboardRepository;
    this.#keymapRepository = keymapRepository;
  }

  async loadKeyboardNames({
    onError,
    onLoading,
    onSuccess,
  }: LoadNamesCallback): Promise<string[] | undefined> {
    onLoading();
    try {
      const names = await this.#keyboardRepository.getKeyboardNames();
      if (!names) {
        onError("No keyboards found");
        return;
      }
      if (onSuccess) {
        onSuccess(names);
      }
      return names;
    } catch (e) {
      console.error(e);
      onError("Error loading keyboards");
    }
  }

  async loadKeyboard(
    name: string,
    { onError, onLoading, onSuccess }: LoadKeyboardCallback,
  ) {
    onLoading();
    try {
      const keyboard = await this.#keyboardRepository.getKeyboard(name);
      if (!keyboard) {
        onError("Keyboard not loaded");
        return;
      }
      if (onSuccess) {
        onSuccess(keyboard);
      }
      return keyboard;
    } catch (e) {
      console.error(e);
      onError("Error loading keyboard");
    }
  }

  async loadKeymapNames({ onError, onLoading, onSuccess }: LoadNamesCallback) {
    onLoading();
    try {
      const names = await this.#keymapRepository.getKeymapNames();
      if (!names) {
        onError("No keymaps found");
        return;
      }
      if (onSuccess) {
        onSuccess(names);
      }
      return names;
    } catch (e) {
      console.error(e);
      onError("Error loading keymaps");
    }
  }

  async loadKeymaps(
    name: string,
    { onError, onLoading, onSuccess }: LoadKeymapCallback,
  ) {
    onLoading();
    try {
      const keymapSet = await this.#keymapRepository.getKeymapSet(name);
      if (!keymapSet) {
        onError("Keymap not loaded");
        return;
      }
      if (onSuccess) {
        onSuccess(keymapSet);
      }
      return keymapSet;
    } catch (e) {
      console.error(e);
      onError("Error loading keymap");
    }
  }
}
