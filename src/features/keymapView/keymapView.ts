import { Component } from "../../components/component";
import { Column } from "../../components/layout/column";
import { Row } from "../../components/layout/row";
import { Keymap } from "../../models/keymap";
import { replaceInDOM } from "../../utils/dom";
import { KeymapRow } from "./keymapRow";

export type KeymapViewProps = {
  keymaps: Keymap[];
  onSelectKeymap?: (keymap: Keymap | undefined) => void;
};

export class KeymapView implements Component {
  #element: HTMLElement = document.createElement("div");
  #rows: KeymapRow[] = [];

  /* active keys to filter the keymaps */
  #filter: string[] | undefined;

  /* keymaps that match the search query */
  #searchMatches: Keymap[] | undefined;
  #onSelectKeymap?: (keymap: Keymap | undefined) => void;

  constructor({ keymaps, onSelectKeymap }: KeymapViewProps) {
    this.#onSelectKeymap = onSelectKeymap;
    this.#rows = keymaps.map(
      (keymap) => new KeymapRow(keymap, this.handleHighlightChange.bind(this)),
    );
    this.rebuild();
  }

  // --------------------------------------------------------------------------
  // Helpers
  // --------------------------------------------------------------------------
  private filteredRows(): KeymapRow[] {
    let rows = [...this.#rows]; // shallow copy

    // Apply filter
    const filter = this.#filter;
    if (filter !== undefined) {
      rows = rows.filter((row) =>
        // Check if all filter keys are present in the keymap
        filter.every((key) =>
          row.keymap.chords.some((chord) => chord.includes(key)),
        ),
      );
    }

    // Apply search matches
    const searchMatches = this.#searchMatches;
    if (searchMatches !== undefined) {
      rows = rows.filter((row) =>
        searchMatches.some((keymap) => keymap.name === row.keymap.name),
      );
    }
    return rows;
  }

  private rebuild() {
    const MAX_ROWS = 10;
    const rows = this.filteredRows();
    const html = Column(rows.slice(0, MAX_ROWS).map((row) => row.html()));
    html.classList.add("keymap-list");

    if (rows.length > MAX_ROWS) {
      const moreText = document.createElement("p");
      moreText.textContent = `+${rows.length - MAX_ROWS} more`;
      moreText.style.fontSize = ".8rem";

      const moreRow = Row([moreText]);
      moreRow.classList.add("keymap-row");
      moreRow.style.justifyContent = "center";

      html.appendChild(moreRow);
    }
    replaceInDOM(this.#element, html);
    this.#element = html;
  }

  // --------------------------------------------------------------------------
  // Event Handlers
  // --------------------------------------------------------------------------
  private handleHighlightChange(rowView: KeymapRow) {
    // Unselect all other rows
    this.#rows.forEach((row) => {
      if (row !== rowView) {
        row.selected = false;
      }
    });

    // Notify the parent component of the highlight change
    if (this.#onSelectKeymap) {
      if (!rowView.selected) {
        this.#onSelectKeymap(undefined);
      } else {
        this.#onSelectKeymap(rowView.keymap);
      }
    }
  }

  // --------------------------------------------------------------------------
  // Public API
  // --------------------------------------------------------------------------
  set filter(filter: string[] | undefined) {
    this.#filter = filter;
    this.rebuild();
  }

  set searchMatches(matches: Keymap[] | undefined) {
    this.#searchMatches = matches;
    this.rebuild();
  }

  html(): HTMLElement {
    return this.#element;
  }
}
