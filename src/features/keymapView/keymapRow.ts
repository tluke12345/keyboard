import { Text } from "../../components/basic/text";
import { Component } from "../../components/component";
import { Row } from "../../components/layout/row";
import { Spacer } from "../../components/layout/spacer";
import { kGap2 } from "../../constants";
import { Keymap } from "../../models/keymap";

export class KeymapRow implements Component {
  #element: HTMLElement;
  #onClick?: (keymap: KeymapRow) => void;
  #selected: boolean = false;
  keymap: Keymap;

  constructor(keymap: Keymap, onClick?: (keymap: KeymapRow) => void) {
    this.keymap = keymap;
    this.#onClick = onClick;
    this.#element = this.buildHTML(keymap);
  }

  // --------------------------------------------------------------------------
  // Helpers
  // --------------------------------------------------------------------------
  private buildHTML(keymap: Keymap): HTMLElement {
    const programElement = Text(keymap.program, { cssClass: "program" });
    const descriptionElement = Text(keymap.name, { cssClass: "description" });
    const keymapElement = Row([
      ...keymap.chords.map(this.buildChordHTML.bind(this)),
    ]);
    keymapElement.classList.add("keymap");

    const element = Row([
      programElement,
      Spacer({ width: kGap2 }),
      descriptionElement,
      Spacer({ width: kGap2 }),
      keymapElement,
    ]);
    element.classList.add("keymap-row");
    element.addEventListener("click", this.handleClick.bind(this));
    return element;
  }

  private buildChordHTML(chord: string[]): HTMLElement {
    const keys = chord.map(this.buildKeyHTML.bind(this));
    const html = Row(keys);
    html.classList.add("keymap-row-chord");
    return html;
  }

  private buildKeyHTML(key: string): HTMLElement {
    const element = document.createElement("div");
    element.classList.add("keyboard-key");
    element.textContent = key;
    return element;
  }

  // --------------------------------------------------------------------------
  // Event Handlers
  // --------------------------------------------------------------------------
  private handleClick() {
    this.selected = !this.selected;
    if (this.#onClick) {
      this.#onClick(this);
    }
  }

  // --------------------------------------------------------------------------
  // Public API
  // --------------------------------------------------------------------------
  set selected(selected: boolean) {
    this.#selected = selected;
    if (selected) {
      this.#element.classList.add("selected");
    } else {
      this.#element.classList.remove("selected");
    }
  }
  get selected(): boolean {
    return this.#selected;
  }

  html(): HTMLElement {
    return this.#element;
  }
}
