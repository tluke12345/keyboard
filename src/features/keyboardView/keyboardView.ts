import { Component } from "../../components/component";
import { Column } from "../../components/layout/column";
import { Row } from "../../components/layout/row";
import { Keyboard } from "../../models/Keyboard";
import { Key } from "../../models/key";
import { KeyboardKey } from "./keyboardKey";

export type KeyboardViewProps = {
  keyboard: Keyboard;
  allKeymapStrings: string[];
  onSelectionChange?: (selected: string[]) => void;
};

export class KeyboardView implements Component {
  #element: HTMLElement;
  #keys: KeyboardKey[];
  #onSelectionChange?: (selected: string[]) => void;

  constructor({
    keyboard,
    allKeymapStrings,
    onSelectionChange,
  }: KeyboardViewProps) {
    const { html, keys } = this.loadLayout(keyboard.layout, allKeymapStrings);
    this.#element = html;
    this.#keys = keys;
    this.#onSelectionChange = onSelectionChange;
  }

  // --------------------------------------------------------------------------
  // Helpers
  // --------------------------------------------------------------------------
  private loadLayout(
    grid: Key[][],
    allKeymapStrings: string[],
  ): {
    html: HTMLElement;
    keys: KeyboardKey[];
  } {
    const keys: KeyboardKey[] = [];
    const rowElements = grid.map((row) => {
      const keyElements = row.map((key) => {
        const enabled =
          allKeymapStrings.includes(key.value) ||
          allKeymapStrings.includes(key.shift ?? "");
        const keyView = new KeyboardKey(
          key,
          enabled,
          this.handleSelectionChange.bind(this),
        );
        keys.push(keyView);
        return keyView.html();
      });
      const rowHTML = Row(keyElements);
      rowHTML.classList.add("keyboard-row");
      return rowHTML;
    });
    const html = Column(rowElements);
    html.classList.add("keyboard");
    return { html, keys: keys };
  }

  // --------------------------------------------------------------------------
  // Event Handlers
  // --------------------------------------------------------------------------
  private handleSelectionChange() {
    if (this.#onSelectionChange) {
      const selected = this.#keys
        .filter((keyView) => keyView.selected)
        .map((keyView) => keyView.key.value);
      this.#onSelectionChange(selected);
    }
  }

  // --------------------------------------------------------------------------
  // Public API
  // --------------------------------------------------------------------------
  set highlight(highlighted: string[]) {
    this.#keys.forEach((keyView) => {
      const valueHighlighted = highlighted.includes(keyView.key.value);
      const shiftHighlighted = keyView.key.shift
        ? highlighted.includes(keyView.key.shift)
        : false;
      keyView.highlighted = valueHighlighted || shiftHighlighted;
    });
  }

  set allKeymapStrings(allKeymapStrings: string[]) {
    this.#keys.forEach((keyView) => {
      const enabled =
        allKeymapStrings.includes(keyView.key.value) ||
        allKeymapStrings.includes(keyView.key.shift ?? "");
      keyView.enabled = enabled;
    });
  }

  html(): HTMLElement {
    return this.#element;
  }
}
