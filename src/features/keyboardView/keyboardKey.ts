import { Component } from "../../components/component";
import { Column } from "../../components/layout/column";
import { Text } from "../../components/basic/text";
import { Key } from "../../models/key";

export class KeyboardKey implements Component {
  #element: HTMLElement;
  #selected: boolean;
  #highlighted: boolean;
  #enabled: boolean = false;

  key: Key;
  onClick?: (key: KeyboardKey) => void;

  constructor(
    key: Key,
    enabled: boolean,
    onClick?: (key: KeyboardKey) => void,
  ) {
    this.key = key;
    this.#selected = false;
    this.#highlighted = false;
    this.onClick = onClick;
    this.#element = this.buildHTML(key, enabled);

    // Set enabled state
    this.enabled = enabled;
  }

  // --------------------------------------------------------------------------
  // Helpers
  // --------------------------------------------------------------------------
  private buildHTML(key: Key, enabled: boolean): HTMLElement {
    const keyContainer = document.createElement(enabled ? "button" : "div");
    keyContainer.classList.add("keyboard-key");
    if (enabled) {
      keyContainer.classList.add("enabled");
    }

    const keyElement = Text(key.label ?? key.value, { cssClass: "value" });
    const shiftElement = Text(key.shiftLabel ?? key.shift ?? "", {
      cssClass: "shift",
    });
    const keyContent = Column([shiftElement, keyElement]);
    keyContainer.appendChild(keyContent);

    if (key.width) {
      keyContainer.style.flex = `${key.width}`;
    }

    keyContainer.addEventListener("click", () => {
      if (this.#enabled && this.onClick) {
        this.selected = !this.#selected;
        this.onClick(this);
      }
    });

    return keyContainer;
  }

  // --------------------------------------------------------------------------
  // Public API
  // --------------------------------------------------------------------------
  get highlighted(): boolean {
    return this.#highlighted;
  }
  set highlighted(value: boolean) {
    if (value === this.#highlighted) {
      return;
    }
    if (value) {
      this.#element.classList.add("highlighted");
    } else {
      this.#element.classList.remove("highlighted");
    }
    this.#highlighted = value;
  }

  // --------------------------------------------------------------------------
  get selected(): boolean {
    return this.#selected;
  }
  set selected(value: boolean) {
    if (value === this.#selected) {
      return;
    }

    if (value) {
      this.#element.classList.add("selected");
    } else {
      this.#element.classList.remove("selected");
    }
    this.#selected = value;
  }

  // --------------------------------------------------------------------------
  set enabled(value: boolean) {
    if (value === this.#enabled) {
      return;
    }
    this.#enabled = value;

    if (value) {
      this.#element.classList.add("enabled");
    } else {
      this.#element.classList.remove("enabled");
    }
  }

  html(): HTMLElement {
    return this.#element;
  }
}
