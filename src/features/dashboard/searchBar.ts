import { SvgElement } from "../../components/basic/svg";
import { Button } from "../../components/buttons/svgButton";
import { Component } from "../../components/component";
import { Row } from "../../components/layout/row";
import { Spacer } from "../../components/layout/spacer";
import { kGap1 } from "../../constants";

export class SearchBar implements Component {
  #element: HTMLElement;

  #onSearch: (query: string | undefined) => void;

  constructor(onSearch: (query: string | undefined) => void) {
    this.#onSearch = onSearch;

    const label = document.createElement("label");
    label.htmlFor = "search";
    label.textContent = "Search";
    label.classList.add("sr-only");

    const searchInput = document.createElement("input");
    searchInput.id = "search";
    searchInput.type = "search";
    searchInput.placeholder = "Search...";
    searchInput.addEventListener("input", () => {
      this.#onSearch(searchInput.value);
    });
    const clearButton = Button({
      htmlElement: SvgElement("icons/close.svg"),
      onClick: () => {
        searchInput.value = "";
        this.#onSearch(undefined);
      },
    });
    clearButton.classList.add("flex-centered");

    const element = Row([
      label,
      searchInput,
      Spacer({ width: kGap1 }),
      clearButton,
    ]);
    element.classList.add("search-view");
    element.style.width = "100%";
    this.#element = element;
  }

  html(): HTMLElement {
    return this.#element;
  }
}
