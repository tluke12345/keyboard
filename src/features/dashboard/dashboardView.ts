import { Text } from "../../components/basic/text";
import { Component } from "../../components/component";
import { Column } from "../../components/layout/column";
import { Spacer } from "../../components/layout/spacer";
import { kGap3 } from "../../constants";
import { Keymap } from "../../models/keymap";
import { replaceInDOM } from "../../utils/dom";
import { fuzzySearch } from "../../utils/search";
import { AppViewModel } from "../../view_models/AppViewModel";
import { KeyboardView } from "../keyboardView/keyboardView";
import { KeymapView } from "../keymapView/keymapView";
import { Navbar } from "./navbar";
import { SearchBar } from "./searchBar";

export class DashboardView implements Component {
  #element: HTMLElement = document.createElement("div");
  #keyboardHTML: HTMLElement;
  #searchHTML: HTMLElement;
  #keymapHTML: HTMLElement;

  #keymaps: Keymap[] = [];
  #keyboardView: KeyboardView | undefined;
  #keymapView: KeymapView | undefined;
  #navbar: Navbar;

  #appViewModel: AppViewModel;

  constructor(appViewModel: AppViewModel) {
    this.#appViewModel = appViewModel;

    this.#keyboardHTML = this.noDataView("Keyboard");
    this.#searchHTML = document.createElement("div");
    this.#keymapHTML = this.noDataView("Keymaps");

    this.#navbar = new Navbar(
      appViewModel,
      this.setKeyboardName.bind(this),
      this.setKeymapsName.bind(this),
    );
    this.rebuild();
  }

  // --------------------------------------------------------------------------
  // Load (async)
  // --------------------------------------------------------------------------
  private rebuild() {
    const html = Column([
      this.#navbar.html(),
      Spacer({ height: kGap3 }),
      this.#keyboardHTML,
      Spacer({ height: kGap3 }),
      this.#searchHTML,
      Spacer({ height: kGap3 }),
      this.#keymapHTML,
    ]);
    html.classList.add("dashboard");
    replaceInDOM(this.#element, html);
    this.#element = html;
  }

  private async loadKeyboard(name: string | undefined) {
    if (!name) {
      this.setKeyboardHTML(this.errorView("No keyboard name provided"));
      return;
    }
    this.#appViewModel.loadKeyboard(name, {
      onLoading: () => this.setKeyboardHTML(this.loadingView()),
      onError: (msg) => this.setKeyboardHTML(this.errorView(msg)),
      onSuccess: (keyboard) => {
        const allKeymapStrings = this.#keymaps.flatMap((keymap) =>
          keymap.chords.flat(),
        );
        this.#keyboardView = new KeyboardView({
          keyboard,
          allKeymapStrings,
          onSelectionChange: this.handleSelectionChange.bind(this),
        });
        this.setKeyboardHTML(this.#keyboardView.html());
      },
    });
  }
  private async loadKeymaps(name: string | undefined) {
    if (!name) {
      this.setKeymapHTML(this.errorView("No keymaps name provided"));
      return;
    }
    this.#appViewModel.loadKeymaps(name, {
      onLoading: () => this.setKeymapHTML(this.loadingView()),
      onError: (msg) => this.setKeymapHTML(this.errorView(msg)),
      onSuccess: (keymapSet) => {
        this.#keymaps = keymapSet.keymaps ?? [];
        if (!this.#keymaps) {
          this.setKeymapHTML(this.errorView("Keymaps not loaded"));
          return;
        }
        if (this.#keyboardView !== undefined) {
          this.#keyboardView.allKeymapStrings = this.#keymaps.flatMap(
            (keymap) => keymap.chords.flat(),
          );
        }
        this.#keymapView = new KeymapView({
          keymaps: this.#keymaps,
          onSelectKeymap: this.handleSelectKeymap.bind(this),
        });
        this.setKeymapHTML(this.#keymapView.html(), true);
      },
    });
  }

  private setKeyboardHTML(html: HTMLElement) {
    replaceInDOM(this.#keyboardHTML, html);
    this.#keyboardHTML = html;
  }

  private setKeymapHTML(html: HTMLElement, isKeymapView: boolean = false) {
    if (isKeymapView) {
      const searchView = new SearchBar(this.handleSearch.bind(this));
      replaceInDOM(this.#searchHTML, searchView.html());
      this.#searchHTML = searchView.html();
    } else {
      const emptyDiv = document.createElement("div");
      replaceInDOM(this.#searchHTML, emptyDiv);
      this.#searchHTML = emptyDiv;
    }
    replaceInDOM(this.#keymapHTML, html);
    this.#keymapHTML = html;
  }

  // --------------------------------------------------------------------------
  // UI
  // --------------------------------------------------------------------------
  private loadingView() {
    const loadingText = Text("Loading...");
    const loadingHTML = Column([loadingText]);
    loadingHTML.classList.add("loading");
    return loadingHTML;
  }

  private errorView(message: string) {
    const errorText = Text(`Error: ${message}`);
    const errorHTML = Column([errorText]);
    errorHTML.classList.add("error");
    return errorHTML;
  }

  private noDataView(name: string) {
    const noDataText = Text(`No ${name} data`);
    const noDataHTML = Column([noDataText]);
    noDataHTML.classList.add("no-data");
    return noDataHTML;
  }

  // --------------------------------------------------------------------------
  // Event Handlers
  // --------------------------------------------------------------------------
  private handleSelectionChange(selected: string[]) {
    if (this.#keymapView) {
      this.#keymapView.filter = selected.length > 0 ? selected : undefined;
    }
  }
  private handleSelectKeymap(keymap: Keymap | undefined) {
    const highlighted = keymap ? keymap.chords.flat() : [];
    if (this.#keyboardView) {
      this.#keyboardView.highlight = highlighted;
    }
  }
  private handleSearch(query: string | undefined) {
    // Fuzzy search
    const matches = query
      ? this.#keymaps.filter((keymap) => {
          return (
            fuzzySearch(query ?? "", keymap.name) ||
            fuzzySearch(query ?? "", keymap.program) ||
            fuzzySearch(query ?? "", keymap.description)
          );
        })
      : this.#keymaps;

    if (this.#keymapView) {
      this.#keymapView.searchMatches = matches;
    }
    if (this.#keyboardView) {
      this.#keyboardView.allKeymapStrings = matches.flatMap((keymap) =>
        keymap.chords.flat(),
      );
    }
  }

  // --------------------------------------------------------------------------
  // Public API
  // --------------------------------------------------------------------------
  setKeyboardName(keyboardName: string) {
    this.loadKeyboard(keyboardName);
  }
  setKeymapsName(keymapsName: string) {
    this.loadKeymaps(keymapsName);
  }

  html() {
    return this.#element;
  }
}
