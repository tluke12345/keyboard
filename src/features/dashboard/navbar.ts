import { ContainerLink } from "../../components/basic/link";
import { SvgElement } from "../../components/basic/svg";
import { Text } from "../../components/basic/text";
import { Component } from "../../components/component";
import { SelectInput } from "../../components/inputs/select";
import { Row } from "../../components/layout/row";
import { FlexSpacer, Spacer } from "../../components/layout/spacer";
import { replaceInDOM } from "../../utils/dom";
import { AppViewModel } from "../../view_models/AppViewModel";

export class Navbar implements Component {
  #element: HTMLElement;
  #selectContainer: HTMLElement;

  #onSetKeyboard: (name: string) => void;
  #onSetKeymap: (name: string) => void;

  constructor(
    appViewModel: AppViewModel,
    onSetKeyboard: (name: string) => void,
    onSetKeymap: (name: string) => void,
  ) {
    this.#onSetKeyboard = onSetKeyboard;
    this.#onSetKeymap = onSetKeymap;
    this.#selectContainer = Text("Loading...");

    this.#element = this.build();
    this.loadNames(appViewModel);
  }

  // --------------------------------------------------------------------------
  // Loading
  // --------------------------------------------------------------------------
  private async loadNames(appViewModel: AppViewModel) {
    const keyboardsPromise = appViewModel.loadKeyboardNames({
      onError: this.onError.bind(this),
      onLoading: () => {},
    });
    const keymapsPromise = appViewModel.loadKeymapNames({
      onError: this.onError.bind(this),
      onLoading: () => {},
    });
    const [keyboards, keymaps] = await Promise.all([
      keyboardsPromise,
      keymapsPromise,
    ]);

    const keyboardNames = ["none", ...(keyboards ?? [])];
    const keymapNames = ["none", ...(keymaps ?? [])];
    this.onSucess(keyboardNames, keymapNames);
  }


  // --------------------------------------------------------------------------
  // Building HTML
  // --------------------------------------------------------------------------
  private onError(msg: string) {
    console.error(msg);
    const html = Text("Error loading names");
    replaceInDOM(this.#selectContainer, html);
    this.#element = html;
  }

  private onSucess(keyboards: string[], keymaps: string[]) {
    const keyboardSelect = SelectInput(
      keyboards,
      "none",
      this.#onSetKeyboard,
    );
    const keymapSelect = SelectInput(keymaps, "none", this.#onSetKeymap);
    const html = Row([keyboardSelect, Spacer({width: "var(--gap1)"}), keymapSelect]);
    replaceInDOM(this.#selectContainer, html);
    this.#element = html;
  }

  private build() {
    const logoIcon = SvgElement("images/k_icon_red.svg");
    logoIcon.classList.add("logo-icon");

    const logoText = Text("Keyboard", { cssClass: "logo-text" });

    const logo = ContainerLink("/", [logoIcon, logoText]);
    logo.style.display = "flex";
    logo.classList.add("logo");

    const element = Row([logo, FlexSpacer(), this.#selectContainer]);
    element.classList.add("navbar");
    return element;
  }

  // --------------------------------------------------------------------------
  // Public API
  // --------------------------------------------------------------------------
  html() {
    return this.#element;
  }
}
