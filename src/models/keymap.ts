export type Chord = string[];

export type Keymap = {
  name: string;
  program: string;
  description: string;
  chords: Chord[];
};

export type KeymapSet = {
  name: string;
  keymaps: Keymap[];
};

