export type Key = {
  // The value that the key represents
  value: string;

  // The shift value for the key
  shift?: string;

  // The label that is printed on the key
  label: string;

  // The label that is printed on the key when shift is pressed
  shiftLabel?: string;

  // The row of the key
  row: number;
  column: number;

  // width mulitplier of the key (default 1, used as 'flex' in css)
  width?: number;
};
