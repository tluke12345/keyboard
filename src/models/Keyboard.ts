import { Key } from "./key";

export type Keyboard = {
  name: string;
  layout: Key[][];
};
