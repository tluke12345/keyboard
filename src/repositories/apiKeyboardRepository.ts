import { apiHost, apiPort } from "../constants";
import { Keyboard } from "../models/Keyboard";
import { convertDefinitionsToKeyboardKeys } from "../utils/keyboard";
import { KeyboardDefinition } from "./definition";
import { KeyboardRepository } from "./keyboardRepository";

const apiRoot = `${apiHost}:${apiPort}/api`;
const namesURL = `${apiRoot}/keyboards/names`;
const keyboardsURL = `${apiRoot}/keyboards`;

export class ApiKeyboardRepository implements KeyboardRepository {
  async getKeyboardNames(): Promise<string[]> {
    try {
      const response = await fetch(namesURL);
      if (!response.ok) {
        throw new Error(`Failed to fetch keymap names: ${response.statusText}`);
      }
      return response.json();
    } catch (error) {
      console.error(error);
      throw new Error(`Failed to fetch keymap names: ${error}`);
    }
  }

  async getKeyboard(name: string): Promise<Keyboard | undefined> {
    try {
      const url = `${keyboardsURL}?name=${name}`;
      const response = await fetch(url, {
        headers: { "Content-Type": "application/json" },
      });
      if (!response.ok) {
        throw new Error(`Failed to fetch keymap set: ${response.statusText}`);
      }
      const definitions = (await response.json()) as KeyboardDefinition;
      return {
        name,
        layout: convertDefinitionsToKeyboardKeys(definitions.layout),
      };
    } catch (error) {
      console.error(error);
      throw new Error(`Failed to fetch keymap set: ${error}`);
    }
  }
}
