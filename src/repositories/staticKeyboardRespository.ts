import { Keyboard } from "../models/Keyboard";
import { convertDefinitionsToKeyboardKeys } from "../utils/keyboard";
import { getMetadata } from "../utils/metadata";
import { KeyboardRepository } from "./keyboardRepository";

// ----------------------------------------------------------------------------
// Data source
// ----------------------------------------------------------------------------
const metadataUrl = "keyboards/metadata.json";
const keyboardBaseUrl = "keyboards";

// ----------------------------------------------------------------------------
// Repository
// ----------------------------------------------------------------------------

/* Repository for keyboard data */
export class StaticKeyboardRepository implements KeyboardRepository {
  #metadata: Record<string, string> | undefined;

  /*
  * Get the names of all the keyboards
  * Error: Throws an error if the metadata cannot be fetched
  */
  async getKeyboardNames(): Promise<string[]> {
    try {
      const metadata = await this.getMetadata();
      return Object.keys(metadata);
    } catch (error) {
      console.error(error);
      throw new Error(`Failed to fetch keyboard names`);
    }
  }

  /*
  * Get a keyboard by name
  * Error: Throws an error if the keyboard cannot be fetched
  */
  async getKeyboard(name: string): Promise<Keyboard | undefined> {
    try {
      const metadata = await this.getMetadata();
      const url = `${keyboardBaseUrl}/${metadata[name]}`;
      return await this.fetchKeyboard(name, url);
    } catch (error) {
      console.error(error);
      throw new Error(`Failed to fetch keyboard: ${name}`);
    }
  }

  /* Get metadata from cache or fetch it */
  private async getMetadata(): Promise<Record<string, string>> {
    const metadata = this.#metadata || await getMetadata(metadataUrl);
    this.#metadata = metadata;
    return this.#metadata;
  }

  /* Fetch a keyboard from a URL */
  private async fetchKeyboard(name: string, url: string): Promise<Keyboard> {
    const response = await fetch(url);
    if (!response.ok) {
      throw new Error(`Failed to fetch keyboard at url: ${url}`);
    }
    const data = await response.json();
    return {
      name,
      layout: convertDefinitionsToKeyboardKeys(data.layout),
    };
  }
}
