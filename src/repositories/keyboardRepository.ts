import { Keyboard } from "../models/Keyboard";

export interface KeyboardRepository {
  getKeyboardNames(): Promise<string[]>;
  getKeyboard(name: string): Promise<Keyboard | undefined>;
}
