import { Keymap, KeymapSet } from "../models/keymap";
import { convertUpperCaseChordToShiftChord } from "../utils/keymaps";
import { getMetadata } from "../utils/metadata";
import { KeymapSetDefinition } from "./definition";
import { KeymapRepository } from "./keymapRepository";


// ----------------------------------------------------------------------------
// Data source
// ----------------------------------------------------------------------------
const metadataUrl = "keymaps/metadata.json";
const keymapsUrl = "keymaps";

// ----------------------------------------------------------------------------
// Repository
// ----------------------------------------------------------------------------

/* Repository for keyboard data */
export class StaticKeymapRepository implements KeymapRepository {
  #metadata: Record<string, string> | undefined;


  async getKeymapNames(): Promise<string[]> {
    try {
      const metadata = await this.getMetadata();
      return Object.keys(metadata);
    } catch (error) {
      console.error(error);
      throw new Error(`Failed to fetch keymap names`);
    }
  }

  async getKeymapSet(name: string): Promise<KeymapSet | undefined> {
    try {
      const metadata = await this.getMetadata();
      const url = `${keymapsUrl}/${metadata[name]}`;
      return await this.fetchKeymapSet(name, url);
    } catch (error) {
      console.error(error);
      throw new Error(`Failed to fetch keymap: ${name}`);
    }
  }

  private async getMetadata(): Promise<Record<string, string>> {
    const metadata = this.#metadata || await getMetadata(metadataUrl);
    this.#metadata = metadata;
    return this.#metadata;
  }

  private async fetchKeymapSet(name: string, url: string): Promise<KeymapSet> {
    const response = await fetch(url);
    if (!response.ok) {
      throw new Error(`Failed to fetch keymap at url: ${url}`);
    }
    const data = (await response.json()) as KeymapSetDefinition;
    const keymaps = data.keymaps.map((keymap: Keymap) => {
      const chords = keymap.chords.map(convertUpperCaseChordToShiftChord);
      return { ...keymap, chords };
    });
    return { name, keymaps };
  }
}
