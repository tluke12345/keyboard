import { KeymapSet } from "../models/keymap";

export interface KeymapRepository {
  getKeymapNames(): Promise<string[]>;
  getKeymapSet(name: string): Promise<KeymapSet | undefined>;
}
