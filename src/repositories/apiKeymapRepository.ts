import { apiHost, apiPort } from "../constants";
import { Keymap, KeymapSet } from "../models/keymap";
import { convertUpperCaseChordToShiftChord } from "../utils/keymaps";
import { KeymapDefinition, KeymapSetDefinition } from "./definition";
import { KeymapRepository } from "./keymapRepository";

const apiRoot = `${apiHost}:${apiPort}/api`;
const namesURL = `${apiRoot}/keymaps/names`;
const keymapURL = `${apiRoot}/keymaps`;

export class ApiKeymapRepository implements KeymapRepository {
  async getKeymapNames(): Promise<string[]> {
    try {
      const response = await fetch(namesURL);
      if (!response.ok) {
        throw new Error(`Failed to fetch keymap names: ${response.statusText}`);
      }
      return response.json();
    } catch (error) {
      console.error(error);
      throw new Error(`Failed to fetch keymap names: ${error}`);
    }
  }
  async getKeymapSet(name: string): Promise<KeymapSet | undefined> {
    try {
      const url = `${keymapURL}?name=${name}`;
      const response = await fetch(url, {
        headers: { "Content-Type": "application/json" },
      });
      if (!response.ok) {
        throw new Error(`Failed to fetch keymap set: ${response.statusText}`);
      }
      const definitions = (await response.json()) as KeymapSetDefinition;
      return {
        name,
        keymaps: definitions.keymaps.map(this.convertKeymapDefinition),
      };
    } catch (error) {
      console.error(error);
      throw new Error(`Failed to fetch keymap set: ${error}`);
    }
  }

  private convertKeymapDefinition(definition: KeymapDefinition): Keymap {
    const chords = definition.chords.map(convertUpperCaseChordToShiftChord);
    return {
      ...definition,
      chords,
    };
  }
}
