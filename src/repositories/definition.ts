export type KeymapSetDefinition = {
  name: string;
  keymaps: KeymapDefinition[];
};
export type KeymapDefinition = {
  name: string;
  program: string;
  description: string;
  chords: string[][];
};

export type KeyDefinition = {
  value: string;
  shift?: string;
  width?: number;
};
export type KeyboardDefinition = {
  name: string;
  layout: KeyDefinition[][];
};
