// --------------------------------------------------------------------------
// Fuzzy Search
// --------------------------------------------------------------------------

/*
 * Fuzzy search
 * @param query - The search query
 * @param target - The target string
 * @returns Whether the query is a fuzzy match for the target
 *
 * Determines if target contains all the characters in query in order.
 * They do not have to be adjacent.
 */
export const fuzzySearch = (query: string, target: string) => {
  if (query.length === 0 || target.length === 0) {
    return false;
  }
  const chars = query.toLowerCase().split("");
  let remaining = target.toLowerCase();
  for (const char of chars) {
    const index = remaining.indexOf(char);
    if (index === -1) {
      return false;
    }
    remaining = remaining.slice(index + 1);
  }
  return true;
};
