import { keyToLabelMap } from "../constants";
import { Key } from "../models/key";
import { KeyDefinition } from "../repositories/definition";

/*
 * Convert a keyboard definition to a grid of keys.
 * @param keyDefinition - The keyboard definition to convert.
 * @returns The grid of keys. (used in Keyboard.layout)
 */
export function convertDefinitionsToKeyboardKeys(keyDefinition: KeyDefinition[][]): Key[][] {
  const grid: Key[][] = [];
  const _add = (key: Key, row: number, column: number) => {
    if (!grid[row]) {
      grid[row] = [];
    }
    grid[row][column] = key;
  };

  keyDefinition.forEach((row, rowIndex) => {
    return row.map((keyDefinition, columnIndex) => {
      const key: Key = {
        value: keyDefinition.value,
        shift: keyDefinition.shift,
        label: labelForKey(keyDefinition.value),
        shiftLabel: labelForKey(keyDefinition.shift),
        row: rowIndex,
        column: columnIndex,
        width: keyDefinition.width || 1,
      };
      _add(key, rowIndex, columnIndex);
    });
  });
  return grid;
}

/*
 * Convert a key value to a human-readable label.
 * If the key value is not found in the keyToLabelMap, return the key value.
 * @param value - The key value to convert.
 * @returns The human-readable label.
 */
export function labelForKey(value?: string): string {
  if (value === undefined) {
    return "";
  }
  return keyToLabelMap[value] || value;
}
