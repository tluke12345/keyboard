import { Chord } from "../models/keymap";

const shift = "Shift";
const upperToLowerMap: Record<string, string> = {
  A: "a",
  B: "b",
  C: "c",
  D: "d",
  E: "e",
  F: "f",
  G: "g",
  H: "h",
  I: "i",
  J: "j",
  K: "k",
  L: "l",
  M: "m",
  N: "n",
  O: "o",
  P: "p",
  Q: "q",
  R: "r",
  S: "s",
  T: "t",
  U: "u",
  V: "v",
  W: "w",
  X: "x",
  Y: "y",
  Z: "z",
};

export function convertUpperCaseChordToShiftChord(chord: Chord): Chord {
  const uppercase = chord.filter((key) => upperToLowerMap[key] !== undefined);
  if (uppercase.length === 0) {
    return chord;
  }
  const lowercase = uppercase.map((key) => upperToLowerMap[key]);

  // ignore shift, any convertable keys, and any keys that are already present
  const otherKeys = chord.filter(
    (key) =>
      key !== shift &&
      upperToLowerMap[key] === undefined &&
      !lowercase.includes(key),
  );
  const newChord = [shift, ...lowercase, ...otherKeys];
  return newChord;
}
