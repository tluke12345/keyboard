/*
 * Fetche a "metadata" file from a URL
 * @param url - The URL of the metadata file
 * @returns The metadata as a record
 * @throws Error: Throws an error if the metadata cannot be fetched
 */
export async function getMetadata(
  url: string,
): Promise<Record<string, string>> {
  const response = await fetch(url);
  if (!response.ok) {
    throw new Error(`Failed to fetch keyboard metadata`);
  }
  return await response.json();
}
