/*
 * Update the DOM with a new element, replacing the existing one if it exists.
 * @param existing The existing element to replace.
 * @param replacement The new element (undefined to remove the existing element)
 */
export function replaceInDOM(
  existing?: HTMLElement,
  replacement?: HTMLElement,
) {
  if (existing && existing.parentElement && replacement) {
    existing.replaceWith(replacement);
  }
  if (existing && existing.parentElement && !replacement) {
    existing.remove();
  }
}
