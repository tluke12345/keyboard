import { DashboardView } from "./features/dashboard/dashboardView";
import { StaticKeyboardRepository } from "./repositories/staticKeyboardRespository";
import { StaticKeymapRepository } from "./repositories/staticKeymapRepository";
import { AppViewModel } from "./view_models/AppViewModel";

// Get the root element and fail spectacularly if it doesn't exist
const rootId = "app";
const root = document.getElementById(rootId);
if (!root) {
  throw new Error(`Unable to find root element with id: '${rootId}'`);
}

/*
 * AppViewModel is the main view model for the application
 * Managed state:
 * - keyboardRepository
 * - keymapRepository
 *
 * We could (althouth I don't recommend it) access the view model directly as
 * it is a global variable. 
 */
export const appViewModel = new AppViewModel(
  new StaticKeyboardRepository(),
  new StaticKeymapRepository(),
);

// Set App as the root element
export const dashboardView = new DashboardView(appViewModel);
root.appendChild(dashboardView.html());

