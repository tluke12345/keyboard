import { expect } from "vitest";

export const expectInnerHTML = (element: HTMLElement, text: string) => {
  // Testing innerHTML is not easy!
  // expect(element.innerHTML).toBe(text) will not work
  //
  // this implementation ASSUMES some behavior of JSON.stringify.
  // HTMLElment (any type) will serialize to { innerText: "some text", ... }
  // I used that behavior to extract the innerText value
  //
  // NOTE: this behavior is testing (vitest?) specific! FRAGILE!
  const json = JSON.stringify(element);
  const rehydrate = JSON.parse(json);
  expect(rehydrate.innerText).toBe(text);
};
