import { describe, it } from "vitest";
import { Text } from "../../../src/components/basic/text";
import { expectInnerHTML } from "../../testUtils/highExpectations";

describe("Text", () => {
  it("should create a new Text element", () => {
    const text = "Hello, World!";
    const html = Text(text);
    expectInnerHTML(html, "Hello, World!");
  });
});
