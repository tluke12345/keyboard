import { describe, expect } from "vitest";
import {
  applyClasses,
} from "../../src/components/classes";

describe("classes", (it) => {
  it("should apply the classes to the element", () => {
    const element = document.createElement("div");
    const classes = ["class1", "class2", ""];

    applyClasses(element, classes);
    expect(element.classList.length).toBe(2);
    expect(element.classList.contains(classes[0])).toBe(true);
    expect(element.classList.contains(classes[1])).toBe(true);
  });
});
