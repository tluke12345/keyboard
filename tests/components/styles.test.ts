import { describe, expect } from "vitest";
import { applyStyles } from "../../src/components/styles";

describe("attributes", (it) => {
  it("should apply only a single style to the element", () => {
    const element = document.createElement("div");
    const style = { color: "red" };

    applyStyles(element, style);
    expect(element.style.color).toBe(style.color);
  });

  it("should apply multiple styles to the element", () => {
    const element = document.createElement("div");
    const style = {
      color: "red",
      backgroundColor: "blue",
    };

    applyStyles(element, style);
    expect(element.style.color).toBe(style.color);
    expect(element.style.backgroundColor).toBe(style.backgroundColor);
  });

  it("should remove a style from the element", () => {
    const element = document.createElement("div");
    element.style.color = "red";
    element.style.backgroundColor = "blue";
    const style = { color: "", backgroundColor: null };

    applyStyles(element, style);

    expect(element.style.color).toBe("");
    expect(element.style.backgroundColor).toBe("");
  });
});
