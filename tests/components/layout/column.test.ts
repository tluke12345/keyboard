import { describe, expect, it } from "vitest";
import { Column } from "../../../src/components/layout/column";

describe("column", () => {
  // Intentianally not many tests here.
  // It should be tested in the integration with larger components.

  it("should pass", () => {
    const row = Column([]);
    expect(row).toBeDefined();
  });
});
