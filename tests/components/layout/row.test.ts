import { describe, expect, it } from "vitest";
import { Row } from "../../../src/components/layout/row";

describe("row", () => {
  // Intentianally not many tests here.
  // It should be tested in the integration with larger components.

  it("should pass", () => {
    const row = Row([]);
    expect(row).toBeDefined();
  });
});
