import { describe, expect, it } from "vitest";
import { fuzzySearch } from "../../src/utils/search";

describe("search", () => {
  it("should return false for empty query string", () => {
    expect(fuzzySearch("", "hello")).toBe(false);
  });

  it("should return false for empty target string", () => {
    expect(fuzzySearch("", "")).toBe(false);
  });

  it("should return true for exact match", () => {
    expect(fuzzySearch("hello", "hello")).toBe(true);
  });

  it("should return true for partial match", () => {
    expect(fuzzySearch("hello", "hello world")).toBe(true);
  });

  it("should search case-insensitively", () => {
    expect(fuzzySearch("hello", "Hello")).toBe(true);
  });

  it("should return true if all query characters are found in order", () => {
    expect(fuzzySearch("h", "helloworld")).toBe(true);
    expect(fuzzySearch("hl", "helloworld")).toBe(true);
    expect(fuzzySearch("hlo", "helloworld")).toBe(true);
    expect(fuzzySearch("lo", "helloworld")).toBe(true);
  });

  it("should return false if query characters are not found in order", () => {
    expect(fuzzySearch("le", "helloworld")).toBe(false);
  });
});
