import { it, describe, expect } from "vitest";
import { convertUpperCaseChordToShiftChord } from "../../src/utils/keymaps";

describe("convertUpperCaseChordToShiftChord", (it) => {
  it("converts multiple uppercase to lowercase and adds shift, ignores non-uppercase", () => {
    const chord = ["A", "d", "B", "C", "+"];
    expect(convertUpperCaseChordToShiftChord(chord)).toEqual([
      "Shift",
      "a",
      "b",
      "c",
      "d",
      "+",
    ]);
  });
  it("ignores lowercase, special, and symbol keys", () => {
    const nonUppercase = [
      "Backspace",
      "Enter",
      "Shift",
      "Control",
      "Alt",
      "Meta",
      "CapsLock",
      "Alphanumeric",
      "Tab",
      "Escape",
      "ArrowUp",
      "ArrowDown",
      "ArrowLeft",
      "ArrowRight",
      "a",
      "b",
      "c",
      "d",
      "+",
      ";",
    ];
    expect(convertUpperCaseChordToShiftChord(nonUppercase)).toEqual(
      nonUppercase,
    );
  });

  it("does not add additional lowercase keys if already present", () => {
    const chord = ["A", "d", "B", "C", "a"];
    expect(convertUpperCaseChordToShiftChord(chord)).toEqual([
      "Shift",
      "a",
      "b",
      "c",
      "d",
    ]);
  });

  it("handles empty input", () => {
    expect(convertUpperCaseChordToShiftChord([])).toEqual([]);
  });
});
