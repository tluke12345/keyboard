# Project Title

Deyboard Shortcut Reference

## Description

I moved to a Linux laptop as my primary development environment in 2019. To increase my speed, wherever possible, I use keyboard shortcuts and avoid the mouse. window managers shortcuts, vim keybindings, vscode shortcuts, browser shortcuts. The number of new key bindings became overwhelming.

## Getting Started

Set up development environment (git hooks)

'''bash
source scripts/setup-development.sh
'''

Install dependencies

'''bash
npm install
'''

Run the app

'''bash
npm run dev
'''

Run the tests

'''bash
npm run test
'''

Run the linter

'''bash
npm run lint
'''

## Authors

Contributors names and contact info

Travis Luke

## License

This project is licensed under the MIT License - see the LICENSE.md file for details
