const re = /[+-]?\d+(\.\d+)?/g

function parseKeys() {
  root = document.getElementById('keyboard')

  let keyboard = '[\n'
  for (let i = 0; i < root.children.length; i++) {
    const element_row = root.children[i]
    keyboard += '  [\n'
    for (let j = 0; j < element_row.children.length; j++) {
      const element_key = element_row.children[j]
      const test = element_key.attributes.style.textContent.match(re)
      let k = element_key.children[0].id
      if (k === "'") k = "\\'"
      let w = test[0]
      if (w == KEY_WIDTH) w = 'KEY_WIDTH'
      else if (w == FN_WIDTH) w = 'FN_WIDTH'
      if (j < element_row.children.length - 1)
        keyboard += `   { key: '${k}', width: ${w} },\n`
      else keyboard += `   { key: '${k}' },\n`
    }
    keyboard += '  ],\n'
  }
  keyboard += ']'
  console.log(keyboard)
}
