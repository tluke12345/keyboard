import { defineConfig } from "vite";
export default defineConfig({
  build: {
    sourcemap: true,
  },
  test: {
    globals: true,
    environment: "jsdom",
    setupFiles: "scripts/setup-tests.ts",
    include: ["src/**/*.test.ts", "tests/**/*.test.ts"],
    coverage: {
      reporters: ["text", "html"],
      include: ["src/**/*", "tests/**/*"],
    },
  },
  // build: {
  //   rollupOptions: {
  //     input: {
  //       app: "/index.html",
  //     },
  //   },
  // },
  // server: {
  //   open: "/index.html",
  // },
});
